
var softruckApp = angular.module('softruckApp', [
  'ngRoute',
  'softruckControllers'
]);

softruckApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/', {
        templateUrl: 'home.html',
        controller: 'HomeCtrl'
      }).
      when('/anp/data', {
        templateUrl: 'data.html',
        controller: 'AnpCtrl'
      }).
      when('/anp/data/:dataId', {
        templateUrl: 'search.html',
        controller: 'SearchCtrl'
      }).
      otherwise({
        redirectTo: '/'
      });
  }]);